import subprocess
import gzip
import os
import shutil
import argparse

import gi
gi.require_version('AppStream', '1.0')
from gi.repository import AppStream as AS
gi.require_version('Flatpak', '1.0')
from gi.repository import Flatpak as Flatpak

defaultGpg = "mQENBFdaDxkBCADFg5SnlJNKTa+qY4Zqhb89O0PQ/NPx/GCK/LRYu7+OAWqQljuKZ/GZUEWG1Fzz5GbOy+EH+hDYq6ReUAM958jsrz8pemmriAB/iQtwRXVXNQcJmeB8b4e6L96lcKTwZDYNPLO4CuGqPPkEF4CYzs+wjkDR6q6KiqBNelVTjTJUd3VBvQlbBhSGZeCXXnJOIYMf2XXETFv8tuoW3SO9dkC018O+xZgF2WIQjHCZK5uChkyF69i3eAtVPRDRoXetH6eI+2HTznhnv3liCu6b2AXCpu5IlnSeoDy2wgmtmYgwu0KzkivKVvaY7qQYSr0JWGioz9NSEsbrhtCb3G3+XuC1ABEBAAG0RUtERSBGbGF0cGFrIChUaGUgZS1tYWlsIGlzIGEgcHVibGljIG1haWxpbmcgbGlzdCkgPGtkZS1kZXZlbEBrZGUub3JnPokBNwQTAQgAIQUCV1oPGQIbAwULCQgHAgYVCAkKCwIEFgIDAQIeAQIXgAAKCRCOITqGYcRb7d2sCACu29H4jzC8bwDB3MMwYTy8nVfeJtCq1LPLnolFG0WMqDtLeOg2q3PdrjjJSquIuPbHTlq+1HWFrEJ3gJ+X26O8bw0acVWdMXPEuJiuQTd6RdWG1y6QpEqAlBVBQ1vF5vCdXrBed6nWodhQ1vQ0iPMzGh1dEyHI9wOyyF3+PCKhq6NY41cftoQSFeXtYgMUL82gq436gVvvqFockavDV407rZkmJflry+f9nNJrBTpNZijd0hi+eVQ0mty4dGoWPAI+1DcR8349vGHCHsCIRRGj0Dra89XKZXJZJDze7LutP7lcY7W2x0alZJogc++wxACos8NldyOvSLuXdrFaMKW4uQENBFdaDxkBCADCXBI4M1iOAcwuNUBeSl85s0VRzIalN2mlyIFw4401Z+heuUXYRrdSUokk+5ea4WiECxe8qw44kLEUVRTBar5xH2pUmmxjupBadzDhr8/Wa2WLj3O4DGPYRBK1A8zNhtL1safxczZ1EukCnIZzstp9gUBqVvAu5ebe3VcAoMYGuqltgxhOS41zDQ7hGyxx+NNvvhCBxjUOV9hmmCo2u0r4Vq28LXiRctEiCKYmgyDj1Hcq86Vlwp7sJ4V4m1Eyewq8IepMzz3zhMpnFnkd03NE5twP/puIwAArzmcLlUed0WOp0YffPqGQe5+NRIJyWkhaxj7BtK8WAVMPmPkW84u/ABEBAAGJAR8EGAEIAAkFAldaDxkCGwwACgkQjiE6hmHEW+3JxQf/bn24l++Nmjj+Vnzi9xZNPKU9DmAQTxigTTBSRkkTLBqjaJn1C0Wuiago7TDrIqGlvA0H1xSYSxiiAnauvxhTEO0o9WNAhbdLotMk3KrysuW/vE+ZRecDoi/2aYX0ANnRG4jDl/2yXYo0+iH9qADkTHYJyhT3U3MDJDjgAQC03jnYe+9Hc6N801eGw/sQvSjLEGsne+nEWwMmhpj8puVCLEoiSwd76fnhcaJuvOwgrldr0NZV83P5hOMc1ABVUIBfPXZbOqfT45HqEmLQi7K9U/slJgL2EZKHXeJ8xF8jsrLKn3gvQCquQrPPaLiiqvO5nmetFZ8+6m8OZMzax92WHg=="

class Observer:
    def addComponent(self, component):
        pass

    def close(self):
        pass

class PrintFlatpakRef(Observer):
    branch="master"
    reporef="https://distribute.kde.org/kderuntime.flatpakrepo"
    def __init__(self, remote, gpg):
        try:
            os.makedirs("www/refs")
        except:
            pass

        self.url = remote.get_url()
        self.gpg = gpg
        with open("www/refs/"+remote.get_name()+".flatpakrepo", "w") as f:
            f.write("[Flatpak Repo]\n"+
                    "Title="+remote.get_title()+"\n"+
                    "Url=" + remote.get_url()+"\n"+
                    "GPGKey="+self.gpg+'\n'
                    )

        try:
            shutil.copytree(ourremote.get_appstream_dir().get_path()+"/icons", "www/icons")
        except:
            pass

    def addComponent(self, component):
        cleanid = component.get_id()
        if cleanid.endswith(".desktop"):
            cleanid = cleanid[0:-len(".desktop")]

        with open("www/refs/"+cleanid+".flatpakref", "w") as f:
            f.write("[Flatpak Ref]\n"+
                    "Title=" + component.get_name()+'\n'
                    "Name=" + cleanid+'\n'
                    "Branch=" + self.branch+'\n'
                    "Url=" + self.url+'\n'
                    "IsRuntime=False\n"+
                    "RuntimeRepo="+self.reporef + '\n'
                    "GPGKey="+self.gpg+'\n\n'

                    "Homepage="+component.get_url(1)+'\n' # AS_URL_KIND_HOMEPAGE
                    "Comment="+component.get_summary()+'\n'
                    "Description="+component.get_description()+'\n'
                    )

class PrintHtml(Observer):
    def __init__(self, remote):
        self.f = open("www/apps-" + remote.get_name()+ ".html", "w")
        self.f.write("<html>\n<head>\n")
        self.f.write("<title>AppStream remote: "+remote.get_name()+"</title>\n")
        self.f.write("</head>\n<body>\n")
        self.f.write("<h1>"+remote.get_title()+"</h1>\n")
        self.f.write("Repository: "+ remote.get_url() +" \n")
        self.f.write("<br />\n")
        self.f.write("<a href='refs/repo.flatpakref'>Flatpak</a>\n")

    def addComponent(self, component):
        cleanid = component.get_id()
        if cleanid.endswith(".desktop"):
            cleanid = cleanid[0:-len(".desktop")]

        icon = component.get_icon_by_size(128, 128)

        self.f.write("<p>\n")
        self.f.write("<h2>" + component.get_name() + "</h2>\n")
        self.f.write("<p>"+component.get_summary()+"</p>\n")
        if icon:
            self.f.write("<img src='icons/"+icon.get_filename()+"'>\n")
            self.f.write("<br />\n")
        self.f.write("<a href='appstream://"+component.get_id()+"'>AppStream</a>\n")
        self.f.write("<br />\n")
        self.f.write("<a href='refs/"+cleanid+".flatpakref'>Flatpak</a>\n")
        self.f.write("<p>"+component.get_description()+"</p>\n")
        self.f.write("</p>\n")
        self.f.write("<hr>\n")


    def close(self):
        self.f.write("</body>\n")
        self.f.write("</html>")
        self.f.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("remote", help="Remote Name", type=str)
    parser.add_argument("--gpg", help="Remote GPG Key", type=str, default=defaultGpg)
    args = parser.parse_args()

    remoteName = args.remote

    installations = Flatpak.get_system_installations()
    ourremote = None
    for i in installations:
        ourremote = i.get_remote_by_name(remoteName)
        if ourremote:
            break

    path = ourremote.get_appstream_dir().get_path()
    pathXml = path + "/appstream.xml.gz"

    asxml = 0
    with gzip.open(pathXml, 'rb') as f:
        asxml = f.read().decode("utf-8")

    md = AS.Metadata()
    md.set_format_style(2) # AS_FORMAT_STYLE_COLLECTION
    md.parse(asxml, 1) # AS_FORMAT_KIND_XML
    components = md.get_components()

    observers = [PrintFlatpakRef(ourremote, args.gpg), PrintHtml(ourremote)]
    for component in components:
        for obs in observers:
            obs.addComponent(component)

    for obs in observers:
        obs.close()
    #print("la", md)
